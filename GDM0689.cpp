#include "GDM0689.h"

GDM0689::GDM0689(int address) 
{
	_address = LCD_ADDRESS; //address;
	_radiostate = 0;
}

void GDM0689::Init() 
{
	Wire.begin();
	Wire.beginTransmission(LCD_ADDRESS);
	Wire.write(B11001100);  //MODE SET (Command)(10)(LowPower)(Enable)(Bias)  (Mux)
                         //         (1)          (0)       (1)     (О©Bias) (1:4)

	Wire.write(B11100000);  //Device select (Command)(1100)(A2 A1 A0)
                         //              (1)            (0  0  0 )
	Wire.write(B11111000);  //Bank Select (Command)(11110)(Input)(Output)
                         //            (1)             (0)    (0) 
	Wire.endTransmission(); 
	CLS(); 
}

void GDM0689::CLS()
{
  	Wire.beginTransmission(_address);
 	Wire.write(B00000000);  //Data pointer points at adress "000000"

	for( int i = 0; i < 16; i++)
	 Wire.write(NULLDIGIT);

        Wire.endTransmission();
}

void GDM0689::EchoSmall(char * str)
{
}

void GDM0689::EchoBig(char * str)
{
  	Wire.beginTransmission(_address);
 	Wire.write(B00001011);  //Data pointer points at adress "00001011"(9)
	for( int i = 5; i > 2; i--)
	{
	 //if(str[i] == 0) break;	
         Wire.write(_char2bigDigit(str[i]));
	}
        Wire.endTransmission();
  	Wire.beginTransmission(_address);
 	Wire.write(B00010011);  //Data pointer points at adress "00001011"(9)
	for( int i = 2; i >= 0; i--)
	{
	 //if(str[i] == 0) break;	
         Wire.write(_char2bigDigit(str[i]));
	}
        Wire.endTransmission();
}


void GDM0689::EchoTemp(float temp)
{
	char str_temp[6];
	
	byte buffer[4]; 

	/* 4 is mininum width, 2 is precision; float value is copied onto str_temp*/
	dtostrf(temp, 4, 1, str_temp);

	if(temp > 9)
	{
	   buffer[0] = _char2bigDigit(str_temp[3]) | BDP;
	   buffer[1] = _char2bigDigit(str_temp[1]) | BDP;
	   buffer[2] = _char2bigDigit(str_temp[0]);
	}

  	Wire.beginTransmission(_address);
 	Wire.write(B00001011);  //Data pointer points at adress "00001011"(9)

	for( int i = 0; i < 3; i++)
	 Wire.write(buffer[i]);

        Wire.endTransmission();

}

void GDM0689::EchoHum(float hum)
{
	char str_hum[6];
	
	byte buffer[4]; 

	/* 4 is mininum width, 2 is precision; float value is copied onto str_temp*/
	dtostrf(hum, 4, 1, str_hum);

	if(hum > 9)
	{
	   buffer[0] = _char2smallDigit(str_hum[0]);
	   buffer[1] = _char2smallDigit(str_hum[1]);// | SDP;
	   buffer[3] = SDOWN0;//_char2smallDigit(str_hum[3]);
	   buffer[2] = SUP0;
	}
	//sprintf(temperature,"%s F", str_temp);

  	Wire.beginTransmission(_address);
 	Wire.write(B00000000);  //Data pointer points at adress "000000"

	for( int i = 0; i < 4; i++)
	 Wire.write(buffer[i]);

        Wire.endTransmission();

}

void GDM0689::EchoRadioState(byte state)
{
	byte radio = 0;

	if( state == 0)
	{
		if( _radiostate == 4)
			_radiostate = 1;
		else
			_radiostate++;
	}
	else
		_radiostate = state;
	switch( _radiostate )
	{
		case 1: radio = RADIO1;
			break;
		case 2: radio = RADIO2;  
			break;
		case 3: radio = RADIO3;  
			break;
		case 4: radio = RADIO4;  
			break;
	}

	Wire.beginTransmission(LCD_ADDRESS);
	Wire.write(B00001001);  
	Wire.write(radio); 
	Wire.endTransmission();
}

void GDM0689::EchoBatteryState(byte state)
{
	Wire.beginTransmission(LCD_ADDRESS);
	Wire.write(B00001000);  
	Wire.write(state); 
	Wire.endTransmission();
}

byte GDM0689::_char2bigDigit(char c)
{
	byte result;
		switch( c )
		{
			case 'O':; 
			case '0':result = BDIGIT0; 
				 break;
			case '1':result = BDIGIT1; 
				 break;
			case '2':result = BDIGIT2; 
				 break;
			case '3':result = BDIGIT3; 
				 break;
			case '4':result = BDIGIT4; 
				 break;
			case '5':result = BDIGIT5; 
				 break;
			case '6':result = BDIGIT6; 
				 break;
			case '7':result = BDIGIT7; 
				 break;
			case 'B':; 
			case '8':result = BDIGIT8; 
				 break;
			case '9':result = BDIGIT9; 
				 break;
			case 'E':result = BCHARE; 
				 break;
			case 'C':result = BCHARC; 
				 break;
			case 'L':result = BCHARL; 
				 break;
			case 'H':result = BCHARH; 
				 break;
			case 'v':; 
			case 'V':result = BCHARV; 
				 break;
			default:result = NULLDIGIT;
		}

	return result;
}

byte GDM0689::_char2smallDigit(char c)
{
	byte result;
		switch( c )
		{
			case '0':result = SDIGIT0; 
				 break;
			case '1':result = SDIGIT1; 
				 break;
			case '2':result = SDIGIT2; 
				 break;
			case '3':result = SDIGIT3; 
				 break;
			case '4':result = SDIGIT4; 
				 break;
			case '5':result = SDIGIT5; 
				 break;
			case '6':result = SDIGIT6; 
				 break;
			case '7':result = SDIGIT7; 
				 break;
			case '8':result = SDIGIT8; 
				 break;
			case '9':result = SDIGIT9; 
				 break;
			default:result = NULLDIGIT;
		}

	return result;
}
