#include "GDM0689.h"
#include <Wire.h>

// setup LCD
GDM0689 LCD;

void setup() {
  LCD.Init();
  LCD.EchoBig("HELLO"); // some "segment" letters enabled
  LCD.EchoBig("v03");

}

void loop() {

  LCD.CLS(); // you need clear screen before new output
  LCD.EchoRadioState(1);  // 4 levels for radio state
  LCD.EchoHum(33.0);  // show humidity using small digits at top left 
  LCD.EchoTemp(21.5); // show temperaure using big digits
  
  delay(10000);
}
