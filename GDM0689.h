#ifndef gdm0689_h
#define gdm0689_h

#include "Arduino.h"
#include "Wire.h"

const int LCD_ADDRESS = B0111000;	

const byte NULLDIGIT = B00000000;
const byte SDP = B00001000;
const byte SDIGIT0 = B11010111;
const byte SDIGIT1 = B00000110;
const byte SDIGIT2 = B11100011;
const byte SDIGIT3 = B10100111;
const byte SDIGIT4 = B00110110;
const byte SDIGIT5 = B10110101;
const byte SDIGIT6 = B11110101;
const byte SDIGIT7 = B00000111;
const byte SDIGIT8 = B11110111;
const byte SDIGIT9 = B10110111;
const byte SUP0 = B00110011;
const byte SDOWN0 = B11100100;

const byte BDP = B00010000;
const byte BDIGIT0 = B11101011;
const byte BDIGIT1 = B01100000;
const byte BDIGIT2 = B11000111;
const byte BDIGIT3 = B11100101;
const byte BDIGIT4 = B01101100;
const byte BDIGIT5 = B10101101;
const byte BDIGIT6 = B10101111;
const byte BDIGIT7 = B11100000;
const byte BDIGIT8 = B11101111;
const byte BDIGIT9 = B11101101;
const byte BCHARC = B10001011;
const byte BCHARE = B10001111;
const byte BCHARH = B01101110;
const byte BCHARL = B00001011;
const byte BCHARV = B00100011;

const byte RADIO1 = B00001000;
const byte RADIO2 = B00001100;
const byte RADIO3 = B00001110;
const byte RADIO4 = B00001111;
 
const byte BATTERY1 = B00010000;
const byte BATTERY2 = B00010001;
const byte BATTERY3 = B00010011;
const byte BATTERY4 = B00010111;
const byte BATTERY5 = B00011111;

class GDM0689 
{
 public:
	GDM0689(int address = 1);
 	void Init();

	void EchoTemp(float temp);
	void EchoHum(float hum);
	void Echo(float temp, float hum);
	void EchoRadioState(byte state = 0);
	void EchoBatteryState(byte state = BATTERY1);
	void CLS();
	void EchoSmall(char * str);
	void EchoBig(char * str);
 protected:
	int _address;
	int _radiostate;
 	byte _char2bigDigit(char c);
 	byte _char2smallDigit(char c);
};
 
#endif
