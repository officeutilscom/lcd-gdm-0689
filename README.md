#GDM0689(GDC0689)

This is an Arduino library for the GDM0689(GDC0689+Driver) display.

* http://www.good-display.com/products_detail/&productId=240.html
* https://ru.aliexpress.com/item/Segment-lcd-Low-temp-6-digits-HTN-I2C-LCD-Module-GDM0689/32810476890.html2

Simple library to display temperature using big digits and humidity using small digit. Radio level can be displayed also.

##License
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
